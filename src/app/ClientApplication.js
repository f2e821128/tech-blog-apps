"use client";
import { useEffect } from "react";

import db from "../utils/firestore";
import { collection, addDoc } from "firebase/firestore";
import visitorIDGenerator from "../utils/visitorIDGenerator";

export default function ClientApplication({ children }) {
  useEffect(() => {
    visitorIDGenerator()
      .then(async (fingerprint) => {
        if (process.env.NEXT_PUBLIC_ENV === "DEV") {
          console.log({ fingerprint });
        } else {
          await addDoc(collection(db, "visitor"), {
            fingerprint: fingerprint,
            datetime: new Date(),
            timestamp: new Date().getTime(),
            url: window.location.href,
          });
        }
      })
      .catch((err) => console.error(err));
  }, []);

  return children;
}
