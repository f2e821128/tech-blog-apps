// please update required information
const siteMetadata = {
  title: "Tech Blog App",
  author: "Charles Sin | Eugene Yiew",
  headerTitle: "Tech Blog App",
  description: "Tech blog app created with Next.js, Tailwind.css and contentlayer.",
  language: "en-us",
  theme: "system", // system, dark or light
  siteUrl: "https://www.google.com", // your website URL
  siteLogo: "/logo.jpg",
  socialBanner: "/social-banner.png", // add social banner in the public folder
  email: "charlessin1993@gmail.com",
  github: "https://github.com/CharlesSin",
  twitter: "https://www.twitter.com",
  facebook: "https://www.facebook.com/profile.php?id=100013411669312",
  youtube: "https://youtube.com",
  linkedin: "https://www.linkedin.com/in/charles-sin-09a713222/",
  dribbble: "https://dribbble.com",
  locale: "en-US",
};

module.exports = siteMetadata;
