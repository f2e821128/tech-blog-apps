# Tech-Blog-App

分享個人在技術學習上的文章和筆記

文章類別如下：
- [x] DevOps
- [x] Docker
- [x] GKE
- [x] Helm chart
- [x] K8s
- [x] Terraform

## Demo

[Tech-Blog-App](https://eugene-tech-blog-app.vercel.app/)

## Introduction

技術文章分享

## Screenshot

![alt cover](/public/screenshot.jpg)

## Use Technology & Library

- Next 13
- Contentlayer
- Tailwindcss

## Features

- [x] 記錄個人的技能
- [x] 技術筆記
