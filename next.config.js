/** @type {import('next').NextConfig} */

const { withContentlayer } = require("next-contentlayer");

const nextConfig = {
  compiler: {
    removeConsole: true,
  },
  reactStrictMode: false,
};

module.exports = withContentlayer({ ...nextConfig });
